import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:native_shared_pref/native_shared_pref.dart';

void main() {
  const MethodChannel channel = MethodChannel('native_shared_pref');

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    expect(await NativeSharedPref.platformVersion, '42');
  });
}
