import 'dart:async';

import 'package:flutter/services.dart';

class NativeSharedPref {
  static const MethodChannel _methodChannel = const MethodChannel(
      'plugins.insighttimer.co/native_shared_pref/methodChannel');
  static const EventChannel _eventChannel = const EventChannel(
      'plugins.insighttimer.co/native_shared_pref/eventChannel');
  static NativeSharedPref _instance;

  final String _sharedPrefFileName;

  NativeSharedPref([this._sharedPrefFileName]);

  Future<String> getString(String key, [String defaultValue = null]) async {
    if (key == null) throw Exception("'key' must not be null");
    var inputData = <String, String>{
      "key": key,
      "default": defaultValue,
      "fileName": _sharedPrefFileName
    };
    try {
      var invokeMethod =
          await _methodChannel.invokeMethod("getString", inputData);
      return invokeMethod;
    } catch (e) {
      throw (e);
    }
  }

  Future<bool> putString(String key, String value) async {
    if (key == null) throw Exception("'key' must not be null");
    var inputData = <String, String>{
      "key": key,
      "value": value,
      "fileName": _sharedPrefFileName
    };
    try {
      await _methodChannel.invokeMethod("putString", inputData);
      return true;
    } catch (e) {
      print(e.message);
      return false;
    }
  }
}
