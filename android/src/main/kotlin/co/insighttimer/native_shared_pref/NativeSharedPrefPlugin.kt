package co.insighttimer.native_shared_pref

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import io.flutter.plugin.common.EventChannel
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.PluginRegistry.Registrar

class NativeSharedPrefPlugin private constructor() : MethodCallHandler, EventChannel.StreamHandler {
    private var sharedPref: SharedPreferences? = null
    private var preferenceChangeListener: SharedPreferences.OnSharedPreferenceChangeListener? = null
    private lateinit var registrar: Registrar

    companion object {
        @JvmStatic
        fun registerWith(registrar: Registrar) {
            val methodChannel = MethodChannel(registrar.messenger(), "plugins.insighttimer.co/native_shared_pref/methodChannel")
            val eventChannel = EventChannel(registrar.messenger(), "plugins.insighttimer.co/native_shared_pref/eventChannel")
            val pluginInstance = NativeSharedPrefPlugin(registrar)
            methodChannel.setMethodCallHandler(pluginInstance)
            eventChannel.setStreamHandler(pluginInstance)
        }
    }

    constructor(registrar: Registrar) : this() {
        this.registrar = registrar
    }

    override fun onMethodCall(call: MethodCall, result: Result) {
        val arguments = call.arguments as Map<String, Any>
        sharedPref = sharedPref ?: createSharedPref(arguments["fileName"]?.let { it as String })
        when (call.method) {
            "getString" -> {
                return try {
                    val defaultValue: String? = arguments["default"]?.let { it as String }
                    result.success(sharedPref?.getString(extractKey(arguments, "key"), defaultValue))
                } catch (e: Exception) {
                    result.error(e.message, null, e)
                }
            }
            "putString" -> {
                return try {
                    val key = extractKey(arguments, "key")
                    sharedPref?.edit()?.putString(key, arguments["value"] as String)?.apply()
                    result.success(true)
                } catch (e: Exception) {
                    result.error(e.message, null, e)
                }
            }
            "getBoolean" -> {
                return try {
                    return result.success(sharedPref?.getBoolean(extractKey(arguments, "key"), false))
                } catch (e: Exception) {
                    result.error(e.message, null, e)
                }
            }
            "putBoolean" -> {
                return try {
                    val key = extractKey(arguments, "key")
                    result.success(sharedPref?.edit()?.putBoolean(key, arguments["value"] as Boolean)?.apply())
                } catch (e: Exception) {
                    result.error(e.message, null, e)
                }
            }
            else -> result.notImplemented()
        }
    }

    private fun createSharedPref(fileName: String?): SharedPreferences {
        return if (fileName == null) {
            PreferenceManager.getDefaultSharedPreferences(registrar.context())
        } else {
            registrar.context().getSharedPreferences(fileName, Context.MODE_PRIVATE)
        }
    }

    private fun extractKey(args: Map<String, Any>, key: String): String {
        return args[key] as String?
                ?: throw Exception("No parameter passed for $key")
    }

    override fun onListen(p0: Any?, p1: EventChannel.EventSink?) {
        preferenceChangeListener = SharedPreferences.OnSharedPreferenceChangeListener { sharedPreferences, key ->
            emitChanged(sharedPreferences, key, p1)
        }
        sharedPref?.registerOnSharedPreferenceChangeListener(preferenceChangeListener)
    }

    private fun emitChanged(sharedPreferences: SharedPreferences?, key: String?, event: EventChannel.EventSink?, valueType: String? = "string") {
        try {
            when (valueType) {
                "string" -> {
                    val value = sharedPreferences?.getString(key, null)
                    event?.success(mapOf(Pair(key, value)))
                    return
                }
                "boolean" -> {
                    val value = sharedPreferences?.getBoolean(key, false)
                    event?.success(mapOf(Pair(key, value)))
                    return
                }
                else -> event?.error("Unsupported value type", null, null)
            }
        } catch (e: ClassCastException) {
            val nextValueType = if (valueType == "string") "boolean" else "other"
            emitChanged(sharedPreferences, key, event, nextValueType)
        }
    }

    override fun onCancel(p0: Any?) {
        sharedPref?.unregisterOnSharedPreferenceChangeListener(preferenceChangeListener)
        preferenceChangeListener = null
    }
}
