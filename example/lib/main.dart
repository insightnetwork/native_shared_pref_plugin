import 'package:flutter/material.dart';
import 'package:native_shared_pref/native_shared_pref.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  NativeSharedPref _pref = NativeSharedPref("settings");
  String text = "nothing yet";

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: Column(
            children: <Widget>[
              Text(text),
              RaisedButton(
                  child: Text('Add to SharedPref'),
                  onPressed: () {
                    _pref.putString("key3", "some value");
                  }),
              RaisedButton(
                  child: Text('Read from SharedPref'),
                  onPressed: () async {
                    var string = await _pref.getString("key3", null);
                    setState(() {
                      text = (string != null) ? string : "null";
                    });
                  })
            ],
          ),
        ),
      ),
    );
  }
}
