#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#
Pod::Spec.new do |s|
  s.name             = 'native_shared_pref'
  s.version          = '0.0.1'
  s.summary          = 'A plugin for reading and writing from&#x2F;to the native (host) app&#x27;s shared preferences from flutter module'
  s.description      = <<-DESC
A plugin for reading and writing from&#x2F;to the native (host) app&#x27;s shared preferences from flutter module
                       DESC
  s.homepage         = 'http://example.com'
  s.license          = { :file => '../LICENSE' }
  s.author           = { 'Your Company' => 'email@example.com' }
  s.source           = { :path => '.' }
  s.source_files = 'Classes/**/*'
  s.public_header_files = 'Classes/**/*.h'
  s.dependency 'Flutter'

  s.ios.deployment_target = '8.0'
end

