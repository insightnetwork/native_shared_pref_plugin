#import "NativeSharedPrefPlugin.h"
#import <native_shared_pref/native_shared_pref-Swift.h>

@implementation NativeSharedPrefPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
 [SwiftNativeSharedPrefPlugin registerWithRegistrar:registrar];
}

@end