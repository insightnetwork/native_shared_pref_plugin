import Flutter
import UIKit
import Foundation

public class SwiftNativeSharedPrefPlugin: NSObject, FlutterPlugin {
  
  let userDefault = UserDefaults.standard
  
  public static func register(with registrar: FlutterPluginRegistrar) {
    let channel = FlutterMethodChannel(name: "plugins.insighttimer.co/native_shared_pref/methodChannel", binaryMessenger: registrar.messenger())
    let instance = SwiftNativeSharedPrefPlugin()
    registrar.addMethodCallDelegate(instance, channel: channel)
  }
  
  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    var arguments = call.arguments as! [String: Any]
    
    switch call.method {
    case "getString":
      guard let key = extractKey(args: arguments, key: "key", result: result) else {
        return
      }
      result(userDefault.value(forKey: key) ?? arguments["default"])
      
    case "putString":
      guard let key = extractKey(args: arguments, key: "key", result: result), let value = arguments["value"] as? String else {
        return
      }
      userDefault.setValue(value, forKey: key)
      
    case "getBoolean":
      guard let key = extractKey(args: arguments, key: "key", result: result) else {
        return
      }
      result(userDefault.bool(forKey: key))
      
    case "putBoolean":
      guard let key = extractKey(args: arguments, key: "key", result: result), let value = arguments["value"] as? Bool else {
        return
      }
      userDefault.set(value, forKey: key)
    default:
      break
    }
  }
  
  private func extractKey(args: [String: Any], key: String, result: FlutterResult) -> String? {
    if let key = args[key] as? String, !key.isEmpty {
      return key
    } else {
      logError(result, message: "Empty or No parameter passed for key")
      return nil
    }
  }
  
  private func logError(_ result: FlutterResult, message: String) {
    result(FlutterError(code: "", message: message, details: nil))
  }
}
