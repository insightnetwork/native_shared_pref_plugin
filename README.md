# native_shared_pref

A plugin for reading and writing from and to the native (host) apps shared preferences from flutter module

## Getting Started

This is a test version and has only methods for getting and setting strings in Shared Preferences.

Use this at your own risk, the author does not accept any liabilities for any outcome resulting from using the plugin.

Please see the basic example to know how to use it.